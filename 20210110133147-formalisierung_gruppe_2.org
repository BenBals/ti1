#+INCLUDE: "base.org" :minlevel 1
#+title: Formalisierung Gruppe 2
#+roam_tags: ti1
#+STARTUP: latexpreview

Das Ende des Monats rückt näher und einige Überweisungen auf dein Konto bzw. von deinem Konto stehen noch aus. Du möchtest deinen Kontostand aber gerne künstlich aufbessern und weißt auch schon wie: Du kennst einen Bankangestellten, der begrenzten Zugriff auf die IT-Systeme hat und in der Lage ist, die Richtung von Überweisungen umzukehren. Er kann also dafür sorgen, dass ausgehende Zahlungen nun stattdessen bei dir eingehen und andersherum. Damit dieser Trick nicht auffällt, möchte er aber nicht mehr als k mal Richtungen umkehren. Zusammen mit deinem aktuellen Guthaben, wie viel Geld kannst du am Ende des Monats mit diesem Trick maximal haben?

