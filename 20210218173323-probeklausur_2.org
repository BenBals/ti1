#+INCLUDE: "base.org" :minlevel 1
#+title: Probeklausur 2
#+roam_tags: ti1
#+STARTUP: latexpreview

[[file:20210204104700-probeklausur_1.org][Vorherige Probeklausur]]

* Aufgabe 1 (20P)
Wir wollen für jedes \(c \in \mathbb{N}\) die Menge \(D_c := \{e \mid |\text{dom}(\varphi_e| \ge c\}\) betrachten, also die Menge der Gödelnummern, deren Programme auf mindestens \(c\) verschiedenen Eingaben halten.

Zeige
1. Für jedes \(c \in \mathbb{N}\) ist \(D_c\) eine Indexmenge.
2. Für jedes \(c \ge 1\) ist \(D_c\) nicht entscheidbar.
3. Für jedes \(c \in \mathbb{N}\) ist \(D_c\) semi entscheidbar.

* Aufgabe 2 (20P)
Zeige:
1. Zu jeder totalen, berechenbaren Funktion \(f\) gibt es eine Gödelnummer \(e\), sodass gilt \(\varphi_e = \varphi_{f(e)}\).
2. Jede nicht leere Indexmenge ist unendlich.
3. Es gibt eine Gödelnummer \(e\), sodass für alle \(x\) gilt, dass \(\varphi_e(x) = e\). (So ein Program wird auch ein Quine genannt.)
   
* Aufgabe 3 (25P)
Gegeben sei eine geordnete Reihe von Büchern mit ihren Seitenzahlen. Diese sollen nur auf eine gegebene
Anzahl $k$ von Lektoren verteilt werden, sodass sie möglichst schnell durchgearbeitet werden. Die Bearbeitungszeit hängt nur von der Seitenzahl eines Buches ab.

Da alle Lektoren gleichzeitig arbeiten, wird die Gesamtzeit also nur vom Lektor mit den meisten Seiten bestimmt.

Um die Logistik einfach zu gestalten, kann jedem Lektor nur ein
zusammenhängender Teil der Bücherreihe gegeben werden. (Die Reihe wird also $k-1$ Stellen geteilt.)

Eine ideale Aufteilung der folgenden Buchreihe auf 3 Lektoren wäre also z. B. $300, 200, 400, 100 \mid 700, 600 \mid 800, 900$.

* Aufgabe 4 (25P)
Wir wollen \(n\) Dateien \(D\) auf einem Magnetband speichern. Wenn wir auf einem Magnetband eine Datei lesen wollen, müssen wir auch alle Dateien, die davor auf dem Band stehen lesen, nicht aber die, die danach sind.

Zu jeder Datei \(i\) wissen, wir wie lang sie ist als Eingabe \(L[i]\) und, wie oft wir sie Lesen wollen \(F[i]\).

Wir suchen nun so eine Sortierung \(\pi\) von \(D\), sodass wir die Lesezeit minimieren.

Dabei ergeben sich die Kosten zum einmaligen Lesen der Datei \(i\) als
\begin{align*}
cost(i) = \sum_{k=1}^{\pi(i)} L[k],
\end{align*}
also als die Summe der Dateilängen vor der Datei \(i\) und der Länge der Datei \(i\) selber.

Die Gesamtkosten einer Datei ergeben sich als die Einzelkosten \(cost(i)\) multipliziert mit der Häufigkeit \(F[i]\).

Insgesamt ergeben sich die Kosten also als
\begin{align*}
\sum_{i=1}^n cost(i) \cdot F[i].
\end{align*}
