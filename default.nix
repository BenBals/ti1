{ fileToBuild ? "", skipPdfs ? ""  }:

with import <nixpkgs> {};

let
    myEmacs = pkgs.emacs26-nox;    
    emacsWithPackages = (pkgs.emacsPackagesGen myEmacs).emacsWithPackages;
in stdenv.mkDerivation rec {
  name = "ti1-tutorium-ben";
  src = ./.;

  buildInputs = [
    (emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [
      org-ref
    ] ++ [
      pkgs.emacs26Packages.let-alist
    ])))
    (texlive.combine {
      inherit (texlive)
        amsmath
        apacite
        beamer
        beamerposter
        biber
        bibtex
        biblatex
        biblatex-apa
        capt-of
        caption
        cite
        collection-fontsrecommended
        collection-langgerman
        commath
        csquotes
        enumitem
        etoolbox
        fancyhdr
        float
        footmisc
        fontspec
        fp
        l3kernel
        l3packages
        newtx
        pgf
        pgfopts
        polyglossia
        scheme-basic
        tikzsymbols
        tikz-cd
        translator
        type1cm
        ulem
        was
        wrapfig
        xcolor
        xetex
        xkeyval;
    })
  ];

  buildPhase = ''
    mkdir -p out

    if [ -z "${fileToBuild}" ]; then
      echo "Building all tuts ++++++++++++++++++++++++++++++++"
      find . -maxdepth 1 -iname "*tutorium*.org" -exec bash build/org-to-beamer.sh {} \;
      bash build/org-to-beamer.sh 20201220165650-zwischentest_post_mortem.org
      bash build/org-to-beamer.sh 20210222172054-fragestunde_vor_der_klausur.org
    else
      echo "Building only matching files ++++++++++++++++++++++++++++++++"
      find . -maxdepth 1 -iname "*${fileToBuild}*.org" -exec bash build/org-to-beamer.sh {} \;
    fi

    if [ "${skipPdfs}" != "true" ]
    then 
      mv *.tex dependencies/src || true
      cp -r static dependencies/src/
      find . -iname "*.tex" -exec bash build/build-and-copy.sh {} \;
    fi

    sh build/org-to-html.sh
    ls -lha
  '';

  installPhase = ''
    mv public $out
    mv out/* $out/ || true
  '';
}
