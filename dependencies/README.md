# HPI LaTeX Slide Master

Contributions are welcome.

If something is missing or does not work as expected, the HPI LaTeX Slide Master might have introduced incompatibilities.
Feel free to create an [issue](https://gitlab.hpi.de/fachschaftsrat/latex/hpi-slides/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) or fix the template yourself and open a [merge request](https://gitlab.hpi.de/fachschaftsrat/latex/hpi-slides/merge_requests/new).

You know a handy trick that might come in useful for others? Create a [snippet](https://gitlab.hpi.de/fachschaftsrat/latex/hpi-slides/snippets)!

## How to beamer

The beamer LaTeX class has a rather extensive [documentation](http://tug.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf).
As always, [tex.stackexchange](https://tex.stackexchange.com/questions/tagged/beamer) is a good place to find answers to all TeX-related problems.

Use [TikZ](http://mirrors.rit.edu/CTAN/graphics/pgf/base/doc/pgfmanual.pdf) to create drawings, which you can also [animate](https://gitlab.hpi.de/fachschaftsrat/latex/hpi-slides/snippets/3).
For beginners, a [shorter introduction](http://cremeronline.com/LaTeX/minimaltikz.pdf) to TikZ is available.
Again, [tex.stackexchange](https://tex.stackexchange.com/questions/tagged/tikz-pgf) probably has the answer to your question.

## Using the Slide Master

### Title Frames

You can make a title frame by adding the option `title` to a frame and generate the title frame contents in it:

```tex
\begin{frame}[title]
    \maketitle
\end{frame}
```

You can adjust the layout with these options:
  - `color` - Specify the color of the title frame box. Defaults to `hpired`, the official ppt-Master also uses `hpiorange`.
  - `bg` - Specify the background image. Defaults to [`Hauptgebaeude_Foyer_II`](backgrounds/Hauptgebaeude_Foyer_II.jpg) and expects the path of an image file relative to the `backgrounds` folder. Use `none` if no image should be rendered.
  - `text` - Specify the title text. Defaults to the presentation title.

*All options are parsed with [pgfkeys](http://mirrors.rit.edu/CTAN/graphics/pgf/base/doc/pgfmanual.pdf#section.82), meaning you have to protect `,` and `=` by wrapping the value in braces:*

```text
\begin{frame}[title={color=hpiorange, bg=Hauptgebaeude_Nacht, text={A long, multiline text\\with , and = inside}}]
    \maketitle
\end{frame}
```

### Title Frames with Small Text Area

The PowerPoint-Master provides a layout for title frames with less text and so do we:
Simply use `title-small` instead of `title` and use the [same options](#title-frames).

## Adding speaker notes

Add the following lines directly before your `\begin{document}` line to add speaker notes:

```tex
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
\setbeameroption{show notes on second screen=right}
```

Then you can add speaker notes to your slides like this:
```
\note{
    \begin{itemize}
        \item Here is the first important note for the slide
        \item Another important note for the slide
    \end{itemize}
}
```

Presenting on a dual-screen setup with these notes works like a charm with [pympress](https://github.com/Cimbali/pympress).
