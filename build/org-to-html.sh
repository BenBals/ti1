#!/usr/bin/env bash
set -euo pipefail
export HOME=$TMP
emacs --batch -l build/html-export.el
