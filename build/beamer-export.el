(require 'ox-beamer)

;; citation
(setq bibtex-dialect 'biblatex)
(require 'org-ref)

;; disable headings
(setq org-latex-title-command "")

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               (add-to-list 'org-latex-classes
                '("beamerhpi"
                  "\\documentclass\[presentation\]\{beamerhpi\}"
                  ("\\section\{%s\}" . "\\section*\{%s\}")
                  ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
                  ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))))

(org-beamer-export-to-latex)
