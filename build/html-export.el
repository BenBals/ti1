(require 'ox-publish)

;; citation
(require 'org-ref)

(setq org-publish-use-timestamps-flag nil)
(setq org-publish-project-alist
      '(
        ("ti1-notes"
         :base-directory "."
         :base-extension "org"
         :publishing-directory "./public"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 4             ; Just the default for this project.
         :html-postamble nil
         :auto-preamble t)
        ("ti1-static"
         :base-directory "static"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|webm"
         :publishing-directory "./public/static"
         :recursive t
         :publishing-function org-publish-attachment )
        ("ti1" :components ("ti1-notes" "ti1-static"))
      ))
(org-publish "ti1")
