#+INCLUDE: "base.org" :minlevel 1
#+title: Probeklausur 1
#+roam_tags: ti1
#+STARTUP: latexpreview

[[file:20210218173323-probeklausur_2.org][Nächste Probeklausur]]

* Aufgabe 1
Wir definieren die Menge \(Const = \{e \mid \forall x,y: \varphi_e(x) = \varphi_e(y) \text{ und beide sind definiert}\}\).

Beweise
1. \(Const\) ist eine Index-Menge.
2. Es gilt \(T \equiv Const\).
   
* Aufgabe 2
Beweise: Ist \(A\) eine nicht triviale, semantische Menge, dann gilt \(H_e \le A\) oder \(\overline{H_e} \le A\).

* Aufgabe 3
Gegeben sei ein Histogram als Array \(A\) von natürlichen Zahlen. Gesucht ist das Rechteck mit der größten Fläche, dass komplett von den Balken des Histograms bedeckt ist.

Oder genauer: Wir suchen Indizes \(1 \le i \le j \le |A|\), sodass das folgende Produkt maximal ist:

\begin{align*}
(j - i + 1) \cdot \min_{i \le k \le j} A[k].
\end{align*}

#+ATTR_LATEX: :height 10cm
#+CAPTION: https://www.geeksforgeeks.org/largest-rectangular-area-in-a-histogram-set-1/
#+ATTR_HTML: :alt Ein Beispiel Histogram :height 200px
[[./static/abb/histogram1.png]]


* Aufgabe 4
Finde einen DP-Algorithmus für das folgende Problem.

Gegeben sei eine Menge \(S\) von natürlichen Zahlen und eine natürliche Zahl \(k\). Gibt es eine Teilmenge \(S'\) von \(S\), sodass \(\sum S' = k\)?
