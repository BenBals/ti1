#+INCLUDE: "base.org" :minlevel 1
#+title: Fragestunde vor der Klausur 
#+roam_tags: ti1
#+STARTUP: latexpreview
#+DATE: 26. Februar 2021

#+HTML: <a href="20210222172054-fragestunde_vor_der_klausur.pdf"">Die Slides zu diesem Tutorium</a>

*  Hundebild
#+ATTR_LATEX: :height 8cm
#+ATTR_HTML: :alt Merlin als Welpe.
[[./static/abb/doof.jpg]]


* Adversary
#+BEGIN_QUOTE
Einmal ist mir beim Adversary nicht ganz klar, ob wir an der Stelle, an der wir bestimmen welche Anfragen der Algorithmus an den Adversary stellen darf quasi für jede Datenstruktur eine richtige Antwort gibt und man die nicht beweisen muss, oder wie sich das generell ergibt.
#+END_QUOTE

Gute Frage! Tatsächlich hängt vom Interface ab, ob der Adversary tatsächlich eine sinnvolle untere Schranke findet. Deshalb müssen wir immer sagen "Vergleichbasiertes Sortieren geht nicht besser als \(\mathcal{O}(n \log n)\)."

Es gibt Standard-Anfragen. Diese stimmen nicht immer, sind aber ein guter Anfang.
- Array: Anfrage auf Index
- Matrix: Anfrage auf Feld
- vergleichbasiert: Anfrage, ob ein Element kleiner/größer ist als ein anderes

* O-Notation
:PROPERTIES:
:BEAMER_opt: allowframebreaks,label=
:END:
#+BEGIN_QUOTE
Die andere Sache ist, dass mir bei den beweisen zur O-Notation relativ unklar ist, was wir da eigentlich so benutzen dürfen. Falls es darauf keine einfach Antwort gibt auch sehr verständlich, aber das würde mir nochmal sehr helfen
#+END_QUOTE

Ihr dürft Euch auf Wissen aus Mathe 1, Mathe 2 und auf (allgemeines) Schulwissen beziehen.

Insbesondere dürft (und sollt auch oft!) die entsprechenden Sätze aus Vorlesung und Übungen benutzen.
** Quellen
- [[https://hpi.de/friedrich/teaching/units/landau.html][Online Unit Landau]]
- [[https://hpi.de/friedrich/teaching/units/rekurrenzen-und-master-theorem.html][Online Unit Master-Theorem]]
** Aufgabe Ü5.1
a. Es gilt, für alle \(a, b > 1\) der Zusammenhang \(\log_a(n) \in \Theta(\log_b(n))\).
b. Für alle \(f,g\) mit \(f \in \mathcal{O}(g)\) gilt \(\mathcal{O}(f(n) + g(n)) = \mathcal{O}(g(n))\).
   

* Heaps
** Heaps — Allgemeine Antwort
:PROPERTIES:
:BEAMER_env: frame
:END:
- Mensch sollte das Interface und die Laufzeit kennen
- Die Techniken [[https://hpi.de/friedrich/teaching/units/heaps/heaps-rework.html][im Beweis]] können relevant sein
- Die innere Arbeitsweise ist vermutlich nicht sehr wichtig.
** Heaps —  Beispielaufgabe
:PROPERTIES:
:BEAMER_env: frame
:END:
Diese Aufgabe entstammt der [[file:20210223135640-probeklausur_3.org][Probeklausur 3]] 

Gegeben sind \(n\) Seile. Jedes Seil hat eine ganzahlige Länge. Wir wollen die Seile jetzt zu einem Seil zusammenfügen. Wir können immer zwei Seile zu einem zusammenfügen. Das kostet uns dann die Summe der beiden Längen.

Haben wir z. B. Seile der Längen 1, 2, 4, 7 und fügen so zusammen:
1. vereinige 1 und 2 zu 3
2. vereinige 4 und 7 zu 11
3. verenige 3 und 11 zu 14,
so kostet uns das 3 + 11 + 14 = 28.

1. Finde eine optimale Reihenfolge für dieses Beispiel.
2. Finde einen Algorithmus, der die optimale Zusammenfüge-Reihenfolge für eine beliebige Menge von Seilen findet.
   
* Rice
** Beweis I
:PROPERTIES:
:BEAMER_env: frame
:END:
Diese Aufgabe enstammt aus der [[file:20210204104700-probeklausur_1.org][Probeklausur 1]].

#+LATEX: \begin{theorem}
#+HTML: <div class="satz"> <b>Satz. </b>
Ist \(A\) eine nicht triviale, semantische Menge, dann gilt \(H_e \le A\) oder \(\overline{H_e} \le A\).
#+HTML: </div>
#+LATEX: \end{theorem}

#+LATEX: \textbf{Beweis.}
#+HTML: <b>Beweis. </b>
Sei \(u\) eine Gödelnummer zur Funktion \(x \mapsto \bot\).

Sei
\begin{align*}
h_{e'}: (e,x) \mapsto \begin{cases}
\varphi_{e'}(x), \text{ falls} \varphi_e(e)\downarrow, \\
\bot, &\text{ sonst}.
\end{cases}
\end{align*}

#+LATEX: \pause

Diese Funktion ist offensichtlich für jedes \(e'\) berechenbar (vgl. Ü2.1(b)). Dann gibt es nach S-m-n-Theorem eine total berechenbare Funktion \(f_{e'}\), sodass für alle \(e,x\) gilt \(\varphi_{f_{e'}(e)}(x) = h_{e'}(e,x)\).

#+LATEX: \pause

Wenn \(\varphi_e' \ne \varphi_u\), sogillt für alle \(e\), dass
1. \(e \in H_e\) genau dann, wenn \(\varphi_{f_{e'}(e)} = \varphi_{e'}\) und
2. \(e \not \in H_e\) genau dann, wenn \(\varphi_{f_{e'}(e)} = \varphi_u\).

**  Beweis II
:PROPERTIES:
:BEAMER_env: frame
:END:
Wir unterscheiden nun zwei Fälle.

*Fall 1:* \(u \in A\). Wählen wir \(e' \not \in A\). (Dann gilt automatisch \(\varphi_{e'} \ne \varphi_u\).) Gilt nun \(\varphi_{f'_{e'}(e)} = \varphi_u\),  so gilt \(f_{e'}(e) \in A\). Gilt \(\varphi_{f'_{e'}(e)} \ne \varphi_u\), so gilt \(\varphi_{f_{e'}(e)} = \varphi_{e'}\), sprich \(f_{e'}(e) \not \in A\).
Also gilt \(e \not \in H_e \iff f_{e'}(e) \in A\). Also gilt \(\overline{H_e} \le A\).

#+LATEX: \pause

*Fall 2:* \(u \not \in A\). Sei \(e' \in A\). (Dann gilt automatisch \(\varphi_{e'} \ne \varphi_u\).) Gilt nun \(\varphi_{f_{e'}(e)} = \varphi_u\), so gilt \(f_{e'}(e) \not \in A\). Gilt nun \(\varphi_{f_{e'}(e)} \ne \varphi_u\), sogilt \(\varphi_{f_{e'}(e)} = \varphi_{e'}\), sprich \(f_{e'}(e) \in A\). 
Also gilt \(e \in H_e \iff f_{e'}(e) \in A\). Also gilt \(H_e \le A\).
#+LATEX: \(\blacksquare\)
#+HTML: ∎

* Material
- [[https://hpi.de/friedrich/moodle/mod/folder/view.php?id=5335][Altklausuren]]
- Probeklausuren
  - [[file:20210204104700-probeklausur_1.org][Probeklausur 1]] 
  - [[file:20210218173323-probeklausur_2.org][Probeklausur 2]] 
  - [[file:20210223135640-probeklausur_3.org][Probeklausur 3]] 
- [[file:alte-klausurvorbereitung.org][Alte Klausurvorbereitung]]
- [[file:20201103160501-aufgabensammlung_s_m_n.org][Aufgabensammlung S-m-n]] 
