#+INCLUDE: "base.org" :minlevel 1
#+title: Tutorium Biene 7 - Viel gelesen
#+roam_tags: ti1
#+STARTUP: latexpreview
#+DATE: 5. Januar 2021

#+HTML: <a href="20210101174508-tutorium_biene_7_viel_gelesen.pdf"">Die Slides zu diesem Tutorium</a> <a href="static/pdf/20210101174508-tutorium_biene_7_viel_gelesen_annotated.pdf"">(mit Notizen)</a>

* Hundebild
#+ATTR_LATEX: :height 8cm
#+ATTR_HTML: :alt Merlin am Strand.
[[./static/abb/merlin-strand.jpg]]

* Wie geht es Dir aktuell mit TI?
  - A ::  Es geht mir super und ich lerne viel!
  - B :: Ich nehme einiges mit und es geht mir gut.
  - C :: Es geht mir ok, bin aber schon manchmal frustriert.
  - D :: TI gehört zu den unangenehmsten Veranstaltungen dieses Semester und ich will einfach nur durchkommen.
  - E :: Mir geht es grauenvoll und ich habe Angst vor TI.
   
*  Wie schwer sind die Übungen?
  - A :: Ich konnte alles einfach runterschreiben.
  - B :: Ich musste etwas nachdenken, habe aber alles schnell hinbekommen.
  - C :: Ich habe mit meinem Kopf die Wand eingeschlagen, bin aber auf der anderen Seite angekommen.
  - D :: Ich hatte gro\ss e Probleme und konnte nicht alles (alleine) lösen.
  - E :: Ich war komplett verzweifelt.

* Lernziele
- Du hast den Inhalt der Online-Units zu Heaps, Sortierverfahren und Greedy verstanden.

* Kernbegriffe
- Algorithmus
- Instanz und Lösung
- Greedy
- Greedy Stays Ahead
- Aus welchen Teilen besteht ein Algorithmusbeweis?

* Fragen zu den Online-Units?

* Schleifeninvarianten - Einfaches Beispiel
#+BEGIN_src
AVG(A):
  res := 0
  for 1 <= i <= |A|:
   res := (res * (i-1) + A[i]) / (i)
  return res
#+END_src

#+LATEX: \pause

*Schleifeninvariante:* Nach jedem Durchlauf \(i\) gilt \(\text{res} =
\text{avg}(A[1], \dots, A[i])\).

* Policepeople catch thieves
** Aufgabenstellung
:PROPERTIES:
:BEAMER_env: frame
:END:
Gegeben ist eine Array indem an jeder Stelle entweder eine Polizistin oder ein Dieb ist. Außerdem ist eine Konstante \(k\) gegeben. Wenn der Abstand zwischen einer Polizistin und einem Dieb maximal \(k\) ist, so kann sie ihn fangen. Jede Polizistin kann maximal einen Dieb fangen. *Wie viele Diebe können gefangen werden*?

#+begin_src 
A = [P, P, P, D, D, P, D, D]
k = 2
#+end_src
Diese Aufgabe entstammt cite:policemen.

** Strategien
:PROPERTIES:
:BEAMER_env: frame
:END:
(Je von links nach rechts ausgeführt.)

- A :: Für jede Polizistin: Wähle den nächsten noch nicht gefangenen Dieb.
- B :: Für jede Polizistin: Wähle den linkesten, erreichbaren und noch nicht gefangenen Dieb.
- C :: Für jede Polizistin: Wähle den rechtesten, erreichbaren und noch nicht gefangenen Dieb.
- D :: Für jede Polizistin: Wähle den erreichbaren Dieb, der von am meisten anderen Polizistinnen erreicht werden kann.

** Lösung
:PROPERTIES:
:BEAMER_env: frame
:END:
- Für jede Polizistin (von links nach rechts):
   - Fange den linkesten, erreichbaren und noch nicht gefangenen Dieb.

#+LATEX: \pause

*Schleifeninvariante:* Nach jeder Polizistin \(p \in [1, |A|]\) gilt: Durch die Polizistin im Bereich \([1,p]\) können nicht mehr Diebe im Bereich \([1, p+k]\) gefangen werden, als unser Algo auswählt.

*Diese Lösung ist falsch.* Eine richtige findet Ihr unter [[file:20210107160927-polizistinnen_und_diebe_ein_greedy_stays_ahead_beweis.org][Polizistinnen und Diebe - Ein greedy stays ahead Beweis]].

** Exkurs: Der Algo in \(\mathcal{O}(n)\)
:PROPERTIES:
:BEAMER_env: frame
:END:
1. Hole die linkeste Polizistin \(p\) und den linkesten Dieb \(d\).
2. So lange es noch unbetrachtete Polizistinnen und Diebe gibt:
   1. Wenn \(|p - d| \le k\). Fängt \(p\) den Dieb \(d\). Finde die nächsten \(p\) und \(d\).
   2. /Andersfalls/, erhöhe \(\min\{p,d\}\) auf den nächsten des korretken Typs.
  
* Wir schauen uns das [[https://hpi.de/friedrich/moodle/pluginfile.php/15504/mod_resource/content/0/homework03.pdf][neue Übungsblatt]] an
#+ATTR_LATEX: :height 10cm
#+CAPTION: http://mathcomics.com/do-dogs-really-enjoy-eating-homework/
#+ATTR_HTML: :alt Hunde sitzen im Restaurant und bestellen Mathehausaufgaben :height 200px
[[./static/abb/dogs-eating-homework.jpg]]

* Teamaufgaben
- bearbeitet die folgenden Aufgaben in Euren Übungsteams

* BO-Raumzuteilung
| Gruppe              | Raum |
|---------------------+------|
| Franziska, Max      |    1 |
| Philip, An          |    2 |
| Johanna, Rieke      |    3 |
| Magarete, Marie     |    4 |
| Siddeskanth, Daniel |    5 |
| Sina, Jerome        |    6 |
| Benjamin, Jeffrey   |    7 |
| Valentin, Florian   |    8 |
| Sebastian, Elena    |  2.1 |
| David, Jessica      |  2.2 |

Die letzten beiden Gruppen wechseln bitte in unseren Zweit-Raum: [[https://bbb.hpi.uni-potsdam.de/b/ben-dvg-0qw-y4v]], da BBB nur 8 BO-Räume unterstützt.

* Fragen zu Heaps
- Wie viele Elemente hat ein Heap der Höhe \(h\) mindestens? Wie viele maximal?
- Wie lange braucht die Operation \(ExtractMax\) auf einem Min-Heap?
- Formuliere eine Schleifeninvariante für die For-Schleife in [[https://hpi.de/friedrich/teaching/units/heaps/heaps-rework#c173057][Zeile 3 des BuildMinHeap-Algos]].

*  Rang ermitteln
Gegeben sei ein unsortiertes Array von Noten und die Note von Emma. Entwickle einen Algorithmus, der herausfindet, wie viele Student:innen bessere Noten erhalten haben als Emma.

Beweise die Korrektheit des Algorithmus mit Hilfe einer Schleifeninvariante.

* Differenz maximieren
Gegeben sei ein Array \(A\) von Integern und eine konstante \(1 \le k \le |A|\). Nun ist eine Aufteilung der Elemente von \(A\) in zwei Subsequenzen \(B\) und \(C\) gesucht, sodass \(|B| = k\) und \(|C| = |A| - k\). Außerdem soll die Differenz

(D. h. jedes Element von \(A\) ist entweder in \(B\) oder in \(C\). Die Reihenfolge ist dabei egal.)

\begin{align*}
\bigg(\sum_{b \in B} b \bigg) - \bigg(\sum_{c \in C} c \bigg)
\end{align*}

Diese Aufgabe entstammt cite:g4ggreedy.

#+INCLUDE: "build/reference_slide.org" :minlevel 1
bibliography:library.bib
